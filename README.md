# How to flash the THAIIOT script

# Hardware Support

1. LoRa32u4 Wifi V.2

# Sensor Support

1. Temp&Humid - AM2320
2. GPS - NEO6MV2
3. Light sensor - BH1750FVI, TSL2561
4. Loudness sensor - LM2904

# Example End device

This is the sample End device design that we use for all testing. 
![]( Hardware/images/End%20device1.jpg)
![]( Hardware/images/End%20device2.jpg)
